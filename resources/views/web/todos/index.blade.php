@extends('web.layouts.default')
@section('content')
<div class="container-fluid">
    <form method="POST" action="/todos">   
    <div class="row">        
        {{ csrf_field() }}
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input-group">
                            <label for="todo"></label>
                            <input type="text" name="note" class="form-control" />
                            <button type="submit" class="btn-submit">Add</button>
                        </div>
                    </div>
                </div>                
            </div>
        </div>        
    </div>
    </form>    
    <div class="row">    
        <div class="col-md-12">
            
                <div class="row">
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Tasks</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($todos as $key => $value) : ?>
                                <tr>
                                    <td>{{ $value->note }}</td>
                                    <td>{{ $value->status_name }}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3">                                            
                                                <form method="POST" action="<?= route('todos.destroy', $value->id) ?>">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-danger delete-todo" value="Delete">
                                                    </div>
                                                </form>
                                                
                                            </div>
                                            <?php if($value->status == $value::STATUS_INCOMPLETE): ?>
                                            <div class="col-md-3">                                            
                                                <form method="get" action="<?= route('todos.complete', $value->id) ?>">
                                                    {{ csrf_field() }}                                                    
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-success complete-todo" value="Mark as Complete">
                                                    </div>
                                                </form>
                                                
                                            </div>   
                                            <?php endif; ?>
                                        </div>                                     
                                    </td>
                                </tr>                                
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('document').ready(function() {
        $('.delete-todo').click(function(e){
            e.preventDefault() 
                if (confirm('Are you sure you want to delete this item?')) {            
                $(e.target).closest('form').submit() 
            }
        });
        $('.complete-todo').click(function(e){
            e.preventDefault() 
                if (confirm('Are you sure you want to complete this item?')) {            
                $(e.target).closest('form').submit() 
            }
        });
    });
</script>
@endsection