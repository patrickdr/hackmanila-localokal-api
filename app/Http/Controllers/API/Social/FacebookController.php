<?php

namespace App\Http\Controllers\API\Social;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Profile;

class FacebookController extends Controller
{
    //
    public function login(Request $request) {
		$data = $request->all();
		try {
			\DB::beginTransaction();
			$stoken = $request->get('token');
			$fbuser = Socialite::driver('facebook')->userFromToken($stoken);
			
			if($fbuser) {
                $user = User::where('email', $fbuser->email)                    
					->first();
                if(!$user) {					
                    $password =  \Hash::make(str_random(8));
                    $user = new User($fbuser->user + ['password' => $password]);	
                    $user->save();
                    $profile = new Profile([
                        'fb_token' => $fbuser->token,
                        'fb_id' => $fbuser->id,
                    ] + $this->_splitName($fbuser->name));
                    $user->profile()->save($profile);
                } 
                $token = JWTAuth::fromUser($user);
			}
			\DB::commit();
			return response()->json(compact('user', 'token', 'fbuser'), 200);

		} catch (\Exception $e) {
            \DB::rollback();
            return response()->json(['error' => $e->getMessage()], 500);			
		}
		return response()->json(['error' => 'forbidden'], 403);
    }
    
    private function _splitName($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return ['first_name' => $first_name, 'last_name' => $last_name];
    }
}
