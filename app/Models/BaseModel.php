<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Validator;

class BaseModel extends Model
{
    /**
     *	Set rules in main model
    *	e.g. App\Models\Project
    */
    public static $rules = [];
	/**
	 *  array $data - fields sent by request
	 *	array $rules - will override the current set of rules
	 *  array $except - exclude field from being validated
	 *  array $override_fields - override rules by fields
	 */
	public static function validator($data, $rules = array(), $except = array(), $override_fields = array()) {
		$rules  = ($rules) ? $rules : static::$rules;
		if($except) {
			foreach($except as $key => $value) {
				unset($rules[$key]);
			}
		}
		if($override_fields) {
			foreach ($override_fields as $key => $value) {
				# code...
				$rules[$key] = $value;
			}
		}
		return Validator::make($data, $rules);
	}  
}
