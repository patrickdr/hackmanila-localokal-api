<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{
    //
    protected $fillable = [
        'fb_id',
        'user_id',
        'fb_token',
        'first_name',
        'last_name'
    ];
    public static $rules = [];

    public function user() {
        return $this->belongsTo('App/User');
    }
}
